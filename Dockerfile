FROM ricardocg94/tf-base:latest

#Install vault 
RUN wget https://releases.hashicorp.com/vault/1.2.3/vault_1.2.3_linux_amd64.zip \
    && unzip vault_1.2.3_linux_amd64.zip \
    && mv vault /usr/bin 

CMD ["/bin/bash"]

